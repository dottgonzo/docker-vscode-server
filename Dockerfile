FROM ubuntu:jammy
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y wget nano iputils-ping curl build-essential
RUN wget -O- https://aka.ms/install-vscode-server/setup.sh  | sh

ENV USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME && useradd --create-home --uid $USER_UID --gid $USER_GID -m $USERNAME

USER $USERNAME

CMD /usr/local/bin/code-server serve-local --accept-server-license-terms --host 0.0.0.0 --without-connection-token
